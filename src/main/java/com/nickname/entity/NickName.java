package com.nickname.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class NickName {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "nick_name")
    private String nickName;
}
