package com.nickname.repo;

import com.nickname.entity.NickName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NickNameRepo extends JpaRepository<NickName,Integer> {

    @Query(value = "SELECT * FROM public.nick_name order by id desc;", nativeQuery = true)
    List<NickName> getNamesAndIds();

}
