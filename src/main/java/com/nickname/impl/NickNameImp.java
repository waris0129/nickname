package com.nickname.impl;

import com.nickname.entity.NickName;
import com.nickname.repo.NickNameRepo;
import com.nickname.service.NickNameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NickNameImp implements NickNameService {

    @Autowired
    private NickNameRepo nickNameRepo;

    @Override
    public List<NickName> getNamesAndIds() {
        return nickNameRepo.getNamesAndIds();
    }

    @Override
    public void saveName(NickName nickName) {
        nickNameRepo.save(nickName);
    }

}
